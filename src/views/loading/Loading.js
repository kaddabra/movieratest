import React, {useEffect} from 'react';
import {Center, Image} from 'native-base';
import loading from '../../assets/images/loading.gif';
import auth from '@react-native-firebase/auth';
import {login} from '../../storage/actions/userToken';
import {useDispatch} from 'react-redux';

const Loading = props => {
  const {navigation} = props;
  const dispatch = useDispatch();

  function onAuthStateChanged(user) {
    if (user) {
      dispatch(login(user.displayName, user.uid, user.email));
      navigation.navigate('Profile');
    } else {
      navigation.navigate('SessionApp');
    }
  }
  useEffect(() => {
    const suscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return suscriber;
  }, []);

  return (
    <Center flex={1} testID="id-loading-test">
      <Image source={loading} style={{width: 80, height: 80}} />
    </Center>
  );
};

export default Loading;
