import React, {useEffect, useState} from 'react';
import {Center, Heading, Stack, Input, Button, Box, Text} from 'native-base';
import {ImageBackgroundScreen} from '../../../components';
import {BackHandler} from 'react-native';
import {inputStyles, buttonStyles, textStyles} from '../../../styles';
import {loadLogin} from '../../../storage/actions/userToken';
import {getErrorSesion} from '../../../storage/selectors';
import {useDispatch, useSelector} from 'react-redux';

const Login = props => {
  const {navigation} = props;
  const [show, setShow] = useState(false);
  const [form, setForm] = useState({email: '', password: ''});
  const dispatch = useDispatch();

  useEffect(() => {
    const returnScreen = () => {
      navigation.navigate('SessionApp');
    };
    BackHandler.addEventListener('hardwareBackPress', returnScreen);
    return () =>
      BackHandler.removeEventListener('hardwareBackPress', returnScreen);
  }, []);

  const handleClick = () => {
    dispatch(loadLogin(form.email, form.password));
    navigation.navigate('Profile');
  };

  const handleClickPass = () => setShow(!show);

  return (
    <ImageBackgroundScreen>
      <Center flex={1} width="80%">
        <Stack space={4} mb="4" mt="3.5">
          <Heading mb="8" size="2xl">
            Iniciar Sesión
          </Heading>
          <Input
            value={form.email}
            onChangeText={text => {
              setForm({...form, email: text});
            }}
            variant="outline"
            size="lg"
            placeholder="Email"
            color="red"
            style={inputStyles.inputBasic}
          />
          <Input
            value={form.password}
            type={show ? 'text' : 'password'}
            overflow="visible"
            style={inputStyles.inputBasic}
            color="red"
            InputRightElement={
              <Button
                roundedLeft="0"
                onPress={handleClickPass}
                style={buttonStyles.heightButton}>
                <Text style={textStyles.boldText}>
                  {show ? 'Hide' : 'Show'}
                </Text>
              </Button>
            }
            placeholder="Password"
            onChangeText={text => {
              setForm({...form, password: text});
            }}
          />
          <Box borderColor="white">
            <Button
              size="md"
              mb="4"
              colorScheme="secondary"
              onPress={handleClick}>
              <Text fontSize="lg" style={textStyles.boldText}>
                Entrar
              </Text>
            </Button>
          </Box>
        </Stack>
      </Center>
    </ImageBackgroundScreen>
  );
};

export default Login;
