import React, {useEffect, useState} from 'react';
import {Center, Heading, Input, Button, Box, Text, Stack} from 'native-base';
import {ImageBackgroundScreen} from '../../../components';
import {BackHandler} from 'react-native';
import {inputStyles, buttonStyles, textStyles} from '../../../styles';
import {useDispatch} from 'react-redux';
import {registerUser} from '../../../storage/actions/userToken';

const Register = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const [show, setShow] = useState(false);
  const [form, setForm] = useState({
    email: '',
    password: '',
    name: '',
    lastname: '',
  });

  useEffect(() => {
    const returnScreen = () => {
      navigation.navigate('SessionApp');
    };
    BackHandler.addEventListener('hardwareBackPress', returnScreen);
    return () =>
      BackHandler.removeEventListener('hardwareBackPress', returnScreen);
  }, []);

  const handlePass = () => setShow(!show);

  const handleClick = () => {
    dispatch(registerUser(form.email, form.password, form.name, form.lastname));
    navigation.navigate('Photo');
  };

  return (
    <ImageBackgroundScreen>
      <Center flex={1}>
        <Stack space={5} mb="4" mt="3.5" direction="column" width="80%">
          <Heading mb="8" size="xl">
            Registro
          </Heading>
          <Input
            value={form.name}
            onChangeText={text => {
              setForm({...form, name: text});
            }}
            variant="outline"
            size="lg"
            placeholder="Nombre"
            color="red"
            style={inputStyles.inputBasic}
          />
          <Input
            value={form.lastname}
            onChangeText={text => {
              setForm({...form, lastname: text});
            }}
            variant="outline"
            size="lg"
            placeholder="Apellido"
            color="red"
            style={inputStyles.inputBasic}
          />
          <Input
            value={form.email}
            onChangeText={text => {
              setForm({...form, email: text});
            }}
            variant="outline"
            size="lg"
            placeholder="Email"
            color="red"
            style={inputStyles.inputBasic}
          />
          <Input
            value={form.password}
            onChangeText={text => {
              setForm({...form, password: text});
            }}
            type={show ? 'text' : 'password'}
            overflow="visible"
            style={inputStyles.inputBasic}
            color="red"
            InputRightElement={
              <Button
                roundedLeft="0"
                onPress={handlePass}
                style={buttonStyles.heightButton}>
                <Text style={textStyles.boldText}>
                  {show ? 'Hide' : 'Show'}
                </Text>
              </Button>
            }
            placeholder="Contraseña"
          />
          <Box borderColor="white">
            <Button
              size="md"
              mb="4"
              colorScheme="secondary"
              onPress={handleClick}>
              <Text fontSize="lg" style={textStyles.boldText}>
                Ingresar
              </Text>
            </Button>
          </Box>
        </Stack>
      </Center>
    </ImageBackgroundScreen>
  );
};

export default Register;
