import React from 'react';
import {Text, Center, Heading, Stack, Box, Button} from 'native-base';
import ImageBackgroundScreen from '../../../components/BackGroundSession/ImageBackgroundScreen';

const SessionMenu = props => {
  const {navigation} = props;
  return (
    <ImageBackgroundScreen>
      <Center flex={1}>
        <Stack space={4} mb="4" mt="3.5">
          <Heading mb="8" size="2xl">
            Moviera Test
          </Heading>
          <Box borderColor="white">
            <Button
              size="md"
              mb="4"
              variant="subtle"
              colorScheme="secondary"
              onPress={() => navigation.navigate('Login')}>
              <Text fontSize="lg">iniciar sesión</Text>
            </Button>
          </Box>
          <Box>
            <Button
              mb="4"
              colorScheme="secondary"
              onPress={() => navigation.navigate('Register')}>
              <Text fontSize="lg">Registrarse</Text>
            </Button>
          </Box>
        </Stack>
      </Center>
    </ImageBackgroundScreen>
  );
};

export default SessionMenu;
