import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import {Camera} from '../../components';
import {getTokenUser} from '../../storage/selectors';

const Photo = props => {
  const {navigation} = props;
  const token = useSelector(state => getTokenUser(state));
  const onSnap = () => {
    navigation.navigate('Profile');
  };

  return (
    <div id="id-camera-test">
      <Camera token={token.token} onSnap={onSnap} />
    </div>
  );
};

export default Photo;
