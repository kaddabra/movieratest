import {Center, Text, Avatar, Button, Input, HStack} from 'native-base';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {getTokenUser, getUserData} from '../../storage/selectors';
import {loadUserData, updateInfo} from '../../storage/actions/userData';
import {signingOut} from '../../storage/actions/userToken';
import {inputStyles, textStyles} from '../../styles';
import {Alert} from 'react-native';
import {PermissionsAndroid} from 'react-native';
import {Camera} from '../../components';

const Profile = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const [edit, setEdit] = useState(false);
  const [form, setForm] = useState({});
  const [isCamera, setIsCamera] = useState(false);
  const token = useSelector(state => getTokenUser(state));
  const data = useSelector(state => getUserData(state));

  useEffect(() => {
    if (token.token) {
      dispatch(loadUserData(token.token));
    }
  }, [token]);

  useEffect(() => {
    if (data) {
      setForm(data);
    }
  }, [data]);

  const handlePhoto = () => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA).then(
      res => {
        if (res !== 'granted') {
          Alert.alert(
            'El permiso de la cámara no está activado',
            'Permita el acceso a su cámara en la opción "Configuración" de su teléfono',
          );
        } else {
          setIsCamera(!isCamera);
        }
      },
    );
  };

  return (
    <Center flex={1}>
      {isCamera && <Camera token={token.token} setIsCamera={setIsCamera} />}
      {edit && (
        <Button
          size="md"
          mb="4"
          marginTop={10}
          colorScheme="secondary"
          onPress={handlePhoto}>
          <Text fontSize="lg" style={textStyles.boldText}>
            {isCamera ? 'Cerrar Camara' : 'Abrir Camara'}
          </Text>
        </Button>
      )}
      {data &&
        (!edit ? (
          <>
            <Avatar
              bg="cyan.500"
              source={{uri: data.urlImagen}}
              size="2xl"
              marginBottom={10}>
              Img
            </Avatar>
            <Text>Nombre: {data.name ?? '--'}</Text>
            <Text>Apellido: {data.lastname ?? '--'}</Text>
            <Text>Edad: {data.age ?? '--'}</Text>
            <Text>Sexo: {data.gender === 'M' ? 'Masculino' : 'Femenino'}</Text>
            <Text>Telefono: {data.phone ?? '--'}</Text>
            <Text>Email: {token.email ?? '--'}</Text>
            <Text>Nacionalidad: {data.nationality ?? '--'}</Text>
            <Text>Ciudad: {data.city ?? '--'}</Text>
            <Text>Dirección: {data.address ?? '--'}</Text>
            <Text>Profesión: {data.profession ?? '--'}</Text>
          </>
        ) : (
          <>
            <HStack alignItems="center">
              <Text style={textStyles.textForm}>Nombre: </Text>
              <Input
                value={form.name}
                onChangeText={text => {
                  const newState = {...form, name: text};
                  setForm(newState);
                }}
                style={inputStyles.form}
                variant="outline"
                size="xl"
                placeholder="nombre"
              />
            </HStack>
            <HStack alignItems="center">
              <Text style={textStyles.textForm}>Apellido: </Text>
              <Input
                value={form.lastname}
                onChangeText={text => {
                  const newState = {...form, lastname: text};
                  setForm(newState);
                }}
                style={inputStyles.form}
                variant="outline"
                size="xl"
                placeholder="nombre"
              />
            </HStack>
            <HStack alignItems="center">
              <Text style={textStyles.textForm}>Edad: </Text>
              <Input
                value={form.age}
                onChangeText={text => {
                  const newState = {...form, age: text};
                  setForm(newState);
                }}
                style={inputStyles.form}
                variant="outline"
                size="xl"
                placeholder="nombre"
              />
            </HStack>
            <HStack alignItems="center">
              <Text style={textStyles.textForm}>Sexo (M/F): </Text>
              <Input
                value={form.gender}
                onChangeText={text => {
                  const newState = {...form, gender: text};
                  setForm(newState);
                }}
                style={inputStyles.form}
                variant="outline"
                size="xl"
                placeholder="nombre"
              />
            </HStack>
            <HStack alignItems="center">
              <Text style={textStyles.textForm}>Telefono: </Text>
              <Input
                value={form.phone}
                onChangeText={text => {
                  const newState = {...form, phone: text};
                  setForm(newState);
                }}
                style={inputStyles.form}
                variant="outline"
                size="xl"
                placeholder="nombre"
              />
            </HStack>
            <HStack alignItems="center">
              <Text style={textStyles.textForm}>Nacionalidad: </Text>
              <Input
                value={form.nationality}
                onChangeText={text => {
                  const newState = {...form, nationality: text};
                  setForm(newState);
                }}
                style={inputStyles.form}
                variant="outline"
                size="xl"
                placeholder="nombre"
              />
            </HStack>
            <HStack alignItems="center">
              <Text style={textStyles.textForm}>Ciudad: </Text>
              <Input
                value={form.city}
                onChangeText={text => {
                  const newState = {...form, city: text};
                  setForm(newState);
                }}
                style={inputStyles.form}
                variant="outline"
                size="xl"
                placeholder="nombre"
              />
            </HStack>
            <HStack alignItems="center">
              <Text style={textStyles.textForm}>Dirección: </Text>
              <Input
                value={form.address}
                onChangeText={text => {
                  const newState = {...form, address: text};
                  setForm(newState);
                }}
                style={inputStyles.form}
                variant="outline"
                size="xl"
                placeholder="nombre"
              />
            </HStack>
            <HStack alignItems="center">
              <Text style={textStyles.textForm}>Profesión: </Text>
              <Input
                value={form.profession}
                onChangeText={text => {
                  const newState = {...form, profession: text};
                  setForm(newState);
                }}
                style={inputStyles.form}
                variant="outline"
                size="xl"
                placeholder="nombre"
              />
            </HStack>
          </>
        ))}

      {edit && (
        <Button
          size="md"
          mb="4"
          marginTop={10}
          colorScheme="secondary"
          onPress={() => {
            dispatch(updateInfo(form, token.token));
            setEdit(false);
          }}>
          <Text fontSize="lg" style={textStyles.boldText}>
            Guardar
          </Text>
        </Button>
      )}
      <Button
        size="md"
        mb="4"
        marginTop={!edit ? 10 : 0}
        colorScheme="secondary"
        onPress={() => setEdit(!edit)}>
        <Text fontSize="lg" style={textStyles.boldText}>
          {!edit ? 'Editar' : 'Cancelar'}
        </Text>
      </Button>
      {!edit && (
        <Button
          size="md"
          mb="4"
          colorScheme="secondary"
          onPress={() => {
            dispatch(signingOut(navigation));
          }}>
          <Text fontSize="lg" style={textStyles.boldText}>
            Cerrar sesión
          </Text>
        </Button>
      )}
    </Center>
  );
};

export default Profile;
