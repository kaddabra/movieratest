export default (state = {}, action) => {
  switch (action.type) {
    case 'LOG_IN':
      return {user: action.user, token: action.token, email: action.email};
    default:
      return state;
  }
};
