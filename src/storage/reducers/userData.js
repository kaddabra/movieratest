export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_USER_DATA':
      return {data: action.userData};
    case 'GET_PHOTO':
      return {data: {...state.data, urlImagen: action.url}};
    default:
      return state;
  }
};
