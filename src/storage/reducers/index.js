import {combineReducers} from 'redux';
import userToken from './userToken';
import userData from './userData';

export default combineReducers({
  userToken,
  userData,
});
