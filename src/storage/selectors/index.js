import {get} from 'lodash';

export const getTokenUser = state => get(state, 'userToken');

export const getErrorSesion = state => get(state, 'userToken.error');

export const getUserData = state => get(state, 'userData.data');
