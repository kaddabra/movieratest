import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';

export function userData(userData) {
  return {
    type: 'GET_USER_DATA',
    userData,
  };
}

export function updatePhoto(url) {
  return {
    type: 'GET_PHOTO',
    url,
  };
}

export const loadUserData = token => async dispatch => {
  try {
    const response = await firestore().collection('users').doc(token).get();
    let url = '';
    if (response) {
      try {
        url = await storage()
          .ref(`/profile_picture/${token}.jpg`)
          .getDownloadURL();
      } catch (err) {
        throw err;
      }
    }
    dispatch(userData({...response._data, urlImagen: url}));
  } catch (err) {
    throw err;
  }
};

export const updateInfo = (data, token) => async dispatch => {
  try {
    await firestore()
      .collection('users')
      .doc(token)
      .update({...data});
    dispatch(userData({...data}));
  } catch (error) {
    throw err;
  }
};

export const updatePhotoAsynd = token => async dispatch => {
  const url = await storage()
    .ref(`/profile_picture/${token}.jpg`)
    .getDownloadURL();
  dispatch(updatePhoto(url));
};
