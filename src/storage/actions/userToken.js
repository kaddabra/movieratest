import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import {userData} from '../../mock/userDataMosck';
import defaultProfilePicture from '../../assets/images/profile_photo.jpg';

export function login(user, token, email) {
  return {
    type: 'LOG_IN',
    user,
    token,
    email,
  };
}

export const loadLogin = (user, pasword) => async dispatch => {
  try {
    const response = await auth().signInWithEmailAndPassword(user, pasword);
    dispatch(login(response.user.displayName, response.user.uid, user));
  } catch (err) {
    throw err;
  }
};

export const registerUser =
  (user, pasword, name, lastname) => async dispatch => {
    try {
      const response = await auth().createUserWithEmailAndPassword(
        user,
        pasword,
      );
      if (response) {
        await firestore()
          .collection('users')
          .doc(response.user.uid)
          .set({...userData, name, lastname});
      }
      dispatch(login(response.user.displayName, response.user.uid, user));
    } catch (err) {
      throw err;
    }
  };

export const signingOut = navigation => async dispatch => {
  try {
    await auth().signOut();
    dispatch(login('', '', ''));
    navigation.navigate('Loading');
  } catch (err) {
    navigation.navigate('Loading');
    throw err;
  }
};
