import React from 'react';
import {render} from '@testing-library/react-native';
import ImageBackgroundScreen from './ImageBackgroundScreen';

test('Render Image Background', async () => {
  const {getAllByTestId} = render(
    <ImageBackgroundScreen>
      <div>example</div>
    </ImageBackgroundScreen>,
  );
  const renderComponent = await getAllByTestId('background-image-test-id');
  expect(renderComponent).toHaveLength(1);
});
