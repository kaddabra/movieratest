import React from 'react';
import styles from './ImageBackgroundScreen.styles';
import backGround from '../../assets/images/background.png';
import {StatusBar, ImageBackground, View} from 'react-native';

const ImageBackgroundScreen = props => {
  const {children} = props;

  return (
    <View style={styles.container} testID="background-image-test-id">
      <StatusBar backgroundColor="indigo" />
      <ImageBackground
        alt="image-background"
        source={backGround}
        resizeMode="cover"
        style={styles.img}>
        {children}
      </ImageBackground>
    </View>
  );
};

export default ImageBackgroundScreen;
