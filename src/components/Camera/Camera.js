import storage from '@react-native-firebase/storage';
import {Text, Button} from 'native-base';
import React, {useRef} from 'react';
import {StyleSheet, View} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {useDispatch} from 'react-redux';
import {updatePhotoAsynd} from '../../storage/actions/userData';

const Camera = ({token, setIsCamera, onSnap}) => {
  const dispatch = useDispatch();
  const camera = useRef(null);
  const reference = storage().ref(`/profile_picture/${token}.jpg`);

  async function takePicture() {
    if (camera) {
      const options = {quality: 0.3, base64: true};
      const data = await camera.current?.takePictureAsync(options);
      const pathToFile = `${data.uri}`;
      await reference.putFile(pathToFile);
      dispatch(updatePhotoAsynd(token));
      onSnap && onSnap();
      setIsCamera && setIsCamera(false);
    }
  }

  return (
    <>
      <View style={styles.container}>
        <RNCamera
          testID="camera-RN"
          ref={camera}
          style={{flex: 1, alignItems: 'center'}}
          captureAudio={false}
          type={1}
          mirrorImage
        />
      </View>
      <View>
        <Button size="md" mb="4" colorScheme="secondary" onPress={takePicture}>
          <Text fontSize="lg" s>
            Tomar Foto
          </Text>
        </Button>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '30%',
    height: '50%',
    flexDirection: 'column',
    backgroundColor: 'black',
  },
});

export default Camera;
