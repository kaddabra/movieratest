import React from 'react';
import 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import store from '../storage/store';

//Components
import {NativeBaseProvider} from 'native-base';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import Login from '../views/session/login/Login';
import SessionMenu from '../views/session/sessionMenu/SessionMenu';
import Register from '../views/session/register/Register';
import Loading from '../views/loading/Loading';
import Profile from '../views/profile/Profile';
import Photo from '../views/photo/Photo';

const AppNavigator = createSwitchNavigator(
  {
    SessionApp: SessionMenu,
    Login,
    Register,
    Loading,
    Profile,
    Photo,
  },
  {
    initialRouteName: 'Loading',
  },
);

const AppContainer = createAppContainer(AppNavigator);

const App = () => {
  return (
    <Provider store={store}>
      <NativeBaseProvider>
        <AppContainer />
      </NativeBaseProvider>
    </Provider>
  );
};

export default App;
