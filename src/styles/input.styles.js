import {StyleSheet} from 'react-native';

const inputStyles = StyleSheet.create({
  inputBasic: {
    borderWidth: 2,
    borderColor: '#6C95FC',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    fontFamily: 'Arvo_Regular',
    fontWeight: 'bold',
    fontSize: 16,
  },
  form: {
    width: '50%',
    marginTop: 4,
    borderWidth: 1,
    borderColor: '#6C95FC',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    fontFamily: 'Arvo_Regular',
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default inputStyles;
