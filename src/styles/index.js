import inputStyles from './input.styles';
import buttonStyles from './button.styles';
import textStyles from './text.styles';

export {inputStyles, buttonStyles, textStyles};
