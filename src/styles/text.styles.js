import {StyleSheet} from 'react-native';

const textStyles = StyleSheet.create({
  boldText: {
    fontFamily: 'Arvo_Regular',
    fontWeight: 'bold',
    fontSize: 16,
  },
  textForm: {
    fontFamily: 'Arvo_Regular',
    fontSize: 16,
    width: '25%',
  },
});

export default textStyles;
