import {StyleSheet} from 'react-native';

const buttonStyles = StyleSheet.create({
  heightButton: {
    height: '100%',
    backgroundColor: '#6C95FC',
  },
});

export default buttonStyles;
